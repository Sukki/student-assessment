import {
  Box,
  Card,
  CardContent,
  CardHeader,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import { Controller, useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import { LoadingButton } from "@mui/lab";
import Link from "next/link";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const inputStyle = { WebkitBoxShadow: "0 0 0 1000px transparent inset" };

const LoginPage = () => {
  const [saveClicked, setsaveClicked] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem("user"))
      window.location.href = `/${localStorage.getItem("user")}/dashboard`;
  });
  const methods = useForm<any>({
    mode: "onChange",
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const { control, handleSubmit, reset, register, getValues } = methods;

  const onSubmit = async (data: object) => {
    const values = getValues();
    setsaveClicked(true);
    axios
      .post("/api/userlogin", { ...values })
      .then((res) => {
        setsaveClicked(false);
        localStorage.setItem("user", res?.data?.data?.role);
        localStorage.setItem(
          "userDetails",
          JSON.parse(JSON.stringify(res?.data?.data))
        );
        router.push(`/${res?.data?.data?.role}/dashboard`);
      })
      .catch((err) => {
        setsaveClicked(false);
        errorMessage(err?.response?.data?.error);
      });
  };

  return (
    <Grid container sx={{ display: "flex", justifyContent: "center", mt: 10 }}>
      <Grid lg={6} sm={12} md={6} item>
        <Toaster />
        <Card
          sx={{
            background: "#F5F6FB",
            padding: 2,
            borderRadius: 2,
            marginBottom: "20px",
            border: `1px solid ${theme}`,
          }}
        >
          <CardHeader
            title={
              <Typography
                gutterBottom
                variant="h4"
                component="div"
                sx={{ textAlign: "center" }}
              >
                {strings.login}
              </Typography>
            }
          />

          <CardContent>
            <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
              <Box>
                <Grid container spacing={4} sx={{ display: "block" }}>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <>
                          <input
                            name="abc"
                            type="text"
                            readOnly={true}
                            style={{ display: "none" }}
                          />
                          <TextField
                            autoComplete="current-password"
                            inputProps={{ style: inputStyle }}
                            id="outlined-basic"
                            label={strings.username}
                            variant="outlined"
                            required
                            fullWidth
                            size="small"
                            {...register(`username`)}
                          />
                        </>
                      )}
                      defaultValue={"new"}
                      name={`username`}
                      control={control}
                    />
                  </Grid>
                  <Grid lg={12} sm={12} md={12} item>
                    <FormControl fullWidth>
                      <Controller
                        render={({ field }) => (
                          <>
                            <input
                              name="abc"
                              type="text"
                              readOnly={true}
                              style={{ display: "none" }}
                            />
                            <TextField
                              autoComplete="new-password"
                              fullWidth
                              inputProps={{ style: inputStyle }}
                              onChange={field.onChange}
                              required
                              type={showPassword ? "text" : "password"}
                              size="small"
                              label={strings.password}
                              sx={{
                                '& input[type="password"]::-ms-reveal': {
                                  display: "none",
                                },
                                '& input[type="password"]::-ms-clear': {
                                  display: "none",
                                },
                              }}
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={handleClickShowPassword}
                                      onMouseDown={handleMouseDownPassword}
                                      edge="end"
                                    >
                                      {showPassword ? (
                                        <Visibility />
                                      ) : (
                                        <VisibilityOff />
                                      )}
                                    </IconButton>
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </>
                        )}
                        name={`password`}
                        control={control}
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </Box>
              <Box sx={{ display: "flex", justifyContent: "center", mt: 3 }}>
                <LoadingButton
                  loading={saveClicked}
                  loadingIndicator={strings.loading}
                  variant="contained"
                  size="large"
                  sx={{
                    background: "black",
                    "&:hover": {
                      backgroundColor: themehover,
                      color: theme,
                    },
                  }}
                  type="submit"
                >
                  {strings.login}
                </LoadingButton>
              </Box>
              <Link
                href={"/register"}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginBottom: 20,
                  marginTop: 20,
                  fontSize: 20,
                }}
              >
                {strings.notamember}
              </Link>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default LoginPage;
