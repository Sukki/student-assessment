import { useRouter } from "next/router";

const App = (props) => {
  const { Component } = props;
  const router = useRouter();
  if (typeof window !== "undefined") {
    const data = localStorage.getItem("userDetails");
    console.log(data, "datadata", router);
    //  if (localStorage.getItem("user"))
    //   window.location.href = `/${localStorage.getItem("user")}/dashboard`;
    if (!data) {
      if (typeof window !== "undefined" && router.pathname != "/") {
        window.location.href = "/";
      }
    } else if (router.pathname === "/_error") {
      // localStorage.removeItem("user");
      // localStorage.removeItem("userDetails");
      if (localStorage.getItem("user"))
        window.location.href = `/${localStorage.getItem("user")}/dashboard`;
      else window.location.href = "/";
    }
  }
  return <Component />;
};

export default App;
