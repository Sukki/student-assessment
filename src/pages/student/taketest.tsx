import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import { useSearchParams } from "next/navigation";
import { Controller, useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import { LoadingButton } from "@mui/lab";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";

const Taketest = () => {
  const [questionsList, setQuestionsList] = useState<any[]>([]);
  const [isLoading, setisLoading] = useState(true);
  const [saveClicked, setsaveClicked] = useState(false);
  const [open, setOpen] = useState(false);
  const [assessmentname, setAssessmentname] = useState("");
  const [count, setCount] = useState(0);
  const router = useRouter();
  const searchParams = useSearchParams();

  const search = searchParams.get("id");

  const methods: any = useForm<any>({
    mode: "onChange",
  });

  const {
    control,
    handleSubmit,
    reset,
    getValues,
    formState: { errors },
  } = methods;

  const handleGet = async () => {
    if (search) {
      const data = {
        id: search,
      };
      axios
        .post("/api/getsets", { ...data })
        .then((res) => {
          setisLoading(false);
          setQuestionsList(res.data.data.questionslist);
          setAssessmentname(res.data.data.assessmentname);
        })
        .catch((err) => {
          setisLoading(false);
          errorMessage("");
        });
    }
  };
  useEffect(() => {
    if (localStorage.getItem("user") === "student") {
      handleGet();
    } else {
      errorMessage("");
      localStorage.removeItem("user");
      localStorage.removeItem("userDetails");
      router.push("/");
    }
  }, [search]);

  const onSubmit = (data: object) => {
    setsaveClicked(true);
    let numbercount = 0;
    const values = getValues();
    const keys = Object.keys(values);
    // toast.success("Assessment submitted successfully");
    for (let i = 0; i < questionsList.length; i++) {
      if (questionsList?.[i]?._id === keys[i]) {
        if (questionsList?.[i]?.answer && values[keys[i]])
          if (questionsList?.[i]?.answer === values[keys[i]]) {
            numbercount += 1;
          }
      }
    }
    setCount(numbercount);
    const datas: any = {
      name: assessmentname,
    };
    axios
      .post("/api/report", { ...datas })
      .then((res) => {
        setsaveClicked(false);
        reset();
        setOpen(true);
      })
      .catch((err) => {
        setsaveClicked(false);
        errorMessage("");
      });
  };

  const handleClose = () => {
    router.replace("/student/dashboard");
    setOpen(false);
  };
  const handleDialog = (count: number) => {
    return (
      <Dialog
        open={open}
        disableEscapeKeyDown
        keepMounted
        maxWidth="sm"
        fullWidth
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle sx={{ color: "green", textAlign: "center" }}>
          {strings.assessmentsubmited}
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-slide-description"
            sx={{ textAlign: "center" }}
          >
            <Typography sx={{ color: "black" }}>
              {strings.totalnoofquestion}
              {questionsList.length}
            </Typography>
            <Typography sx={{ color: "green" }}>
              {strings.correct}
              {count}
            </Typography>
            <Typography sx={{ color: "red" }}>
              {" "}
              {strings.wrong}
              {questionsList.length - count}
            </Typography>
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "center", mb: 3 }}>
          <Button
            variant="contained"
            onClick={handleClose}
            sx={{
              background: theme,
              "&:hover": {
                backgroundColor: themehover,
                color: theme,
              },
            }}
          >
            {strings.oktext}
          </Button>
        </DialogActions>
      </Dialog>
    );
  };
  return (
    <Grid container sx={{ display: "flex", justifyContent: "center", mt: 5 }}>
      <Grid lg={10} sm={12} md={12} item>
        <Toaster />
        <Card sx={{ border: `1px solid ${theme}` }}>
          <CardHeader
            title={
              <Typography gutterBottom variant="h5" component="div">
                {strings.takeassessment}
              </Typography>
            }
            action={
              <Typography
                gutterBottom
                variant="h5"
                component="div"
                sx={{ color: "green", mr: 2 }}
              >
                {questionsList.length >= 1 && questionsList?.length}{" "}
                {strings.questions.toLowerCase()}
              </Typography>
            }
          />
          <Divider />
          {handleDialog(count)}
          <CardContent>
            <form onSubmit={handleSubmit(onSubmit)}>
              {isLoading ? (
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <CircularProgress disableShrink />
                </div>
              ) : questionsList.length >= 1 ? (
                <>
                  {questionsList.map((el, i) => (
                    <Grid key={i} lg={12} sm={12} md={12} item sx={{ mb: 3 }}>
                      <Card sx={{ background: "#F5F6FB" }}>
                        <Controller
                          rules={{
                            required: {
                              value: true,
                              message: strings.pleaseselectvalidanswer,
                            },
                          }}
                          name={el._id}
                          render={({ field }) => (
                            <CardContent sx={{ display: "flex" }}>
                              <FormControl error={Boolean(errors?.[el._id])}>
                                <FormLabel id="demo-radio-buttons-group-label">
                                  {i + 1}. {el.writequestion}
                                </FormLabel>
                                <RadioGroup
                                  row
                                  aria-labelledby="demo-radio-buttons-group-label"
                                  name="radio-buttons-group"
                                >
                                  {el.options.map((e: any, ind: number) => (
                                    <FormControlLabel
                                      key={ind}
                                      value={e.value}
                                      control={
                                        <Radio
                                          onChange={field.onChange}
                                          sx={{
                                            "&.Mui-checked": {
                                              color: theme,
                                            },
                                          }}
                                        />
                                      }
                                      label={e.name}
                                    />
                                  ))}
                                </RadioGroup>
                              </FormControl>
                            </CardContent>
                          )}
                          control={control}
                        />
                        {errors?.[el._id] && (
                          <FormHelperText
                            sx={{ color: "error.main", ml: 3, mb: 2 }}
                          >
                            {errors[el._id]["message"]}
                          </FormHelperText>
                        )}
                      </Card>
                    </Grid>
                  ))}
                  <Box sx={{ display: "flex", justifyContent: "center" }}>
                    <Button
                      variant={"outlined"}
                      sx={{ mb: 5, mr: 5, borderColor: theme, color: theme }}
                      onClick={() => router.push("/student/dashboard")}
                      type="button"
                    >
                      {strings.back}
                    </Button>
                    <LoadingButton
                      loading={saveClicked}
                      loadingIndicator={strings.loading}
                      variant="contained"
                      size="large"
                      sx={{
                        mb: 5,
                        background: theme,
                        "&:hover": {
                          backgroundColor: themehover,
                          color: theme,
                        },
                      }}
                      type="submit"
                    >
                      {strings.submit}
                    </LoadingButton>
                  </Box>
                </>
              ) : (
                <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                  <Card sx={{ background: "#F5F6FB" }}>
                    <CardHeader
                      title={
                        <Typography
                          gutterBottom
                          variant="h5"
                          component="div"
                          sx={{ textAlign: "center" }}
                        >
                          {strings.somethingwentwrong}
                        </Typography>
                      }
                    />
                  </Card>
                </Grid>
              )}
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default Taketest;
