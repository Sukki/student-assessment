import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { Toaster } from "react-hot-toast";
import { useEffect, useState } from "react";
import axios from "axios";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";

const Dashboard = () => {
  const router = useRouter();
  const [isLoading, setisLoading] = useState(true);
  const [viewAssessmentSetData, setviewAssessmentSetData] = useState([]);

  const handleGet = async () => {
    axios
      .get("/api/set")
      .then((res) => {
        setisLoading(false);
        setviewAssessmentSetData(res.data.data);
      })
      .catch((err) => {
        setisLoading(false);
        errorMessage("");
      });
  };
  useEffect(() => {
    if (localStorage.getItem("user") === "student") {
      setisLoading(true);
      handleGet();
    } else {
      errorMessage("");
      localStorage.removeItem("user");
      localStorage.removeItem("userDetails");
      router.push("/");
    }
  }, []);

  return (
    <Box
      sx={{
        width: "85%",
        typography: "body1",
        marginLeft: "auto",
        marginRight: "auto",
        mt: 5,
      }}
    >
      <Card sx={{ border: `1px solid ${theme}` }}>
        <CardHeader
          sx={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
          title={
            <Typography gutterBottom variant="h5" component="div">
              {strings.assessments}
            </Typography>
          }
          action={
            <Button
              variant={"contained"}
              onClick={() => {
                localStorage.removeItem("user");
                localStorage.removeItem("userDetails");
                router.push("/");
              }}
              type="button"
              sx={{
                mr: 3,
                mb: 3,
                background: theme,
                "&:hover": {
                  backgroundColor: themehover,
                  color: theme,
                },
              }}
            >
              {strings.logout}
            </Button>
          }
        />
        <Divider />
        <CardContent>
          <Grid container sx={{ mb: 2 }}>
            <Toaster />
          </Grid>
          <Grid container sx={{ justifyContent: "center" }}>
            {isLoading ? (
              <CircularProgress disableShrink />
            ) : viewAssessmentSetData.length >= 1 ? (
              viewAssessmentSetData.map((el: any, i: number) => (
                <Grid key={i} lg={12} sm={12} md={12} item sx={{ mb: 3 }}>
                  <Card sx={{ background: "#F5F6FB" }}>
                    <CardContent sx={{ display: "flex", flexWrap: "wrap" }}>
                      <Grid lg={10} sm={12} md={10} item>
                        <Typography gutterBottom variant="h5" component="div">
                          {i + 1}. {el.assessmentname} - {el.noofquestions}{" "}
                          {strings.questions.toLowerCase()}
                        </Typography>
                      </Grid>
                      <Grid lg={2} sm={12} md={2} item>
                        <Button
                          sx={{ mr: 5, borderColor: theme, color: theme }}
                          variant={"outlined"}
                          onClick={() =>
                            router.push(`/student/taketest/?id=${el._id}`)
                          }
                          type="button"
                        >
                          {strings.taketest}
                        </Button>
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              ))
            ) : (
              <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                <Card sx={{ background: "#F5F6FB" }}>
                  <CardHeader
                    title={
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        sx={{ textAlign: "center" }}
                      >
                        {strings.noassessment}
                      </Typography>
                    }
                  />
                </Card>
              </Grid>
            )}
          </Grid>
        </CardContent>
      </Card>
    </Box>
  );
};
export default Dashboard;
