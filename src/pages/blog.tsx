import type {
  InferGetStaticPropsType,
  GetStaticProps,
  GetServerSideProps,
  InferGetServerSidePropsType,
} from "next";

type Repo = {
  id: string;
  email: string;
  first_name: string;
};

// export const getServerSideProps = (async (context) => {
//   console.log("get", new Date().toLocaleTimeString());
//   const res = await fetch("https://reqres.in/api/users?page=2");
//   const repo = await res.json();
//   return { props: { repo } };
// }) satisfies GetServerSideProps<{
//   repo: Repo;
// }>;

export async function getStaticProps() {
  console.log("call getstatic");
  // If this request throws an uncaught error, Next.js will
  // not invalidate the currently shown page and
  // retry getStaticProps on the next request.
  const res = await fetch("https://reqres.in/api/users?page=2");
  const repo = await res.json();

  if (!res.ok) {
    // If there is a server error, you might want to
    // throw an error instead of returning so that the cache is not updated
    // until the next successful request.
    throw new Error(`Failed to fetch posts, received status ${res.status}`);
  }

  // If the request was successful, return the posts
  // and revalidate every 10 seconds.
  return {
    props: {
      repo,
    },
    revalidate: 1,
  };
}

export default function Page({
  repo,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  console.log("page", new Date().toLocaleTimeString());
  return (
    <section>
      <h2>Blog</h2>
      <ul>
        {repo ? (
          repo.data.map(({ id, email, first_name }: Repo) => (
            <li key={id}>
              {email}
              <br />
              {id}
              <br />
              <button onClick={() => console.log("call")}>{first_name}</button>
            </li>
          ))
        ) : (
          <p>loading</p>
        )}
      </ul>
    </section>
  );
}
