import type { InferGetStaticPropsType } from "next";
import { useEffect, useState } from "react";

type Repo = {
  id: string;
  email: string;
  first_name: string;
};

export default function Page() {
  console.log("page", new Date().toLocaleTimeString());
  const [repo, setRepo] = useState<any>({ data: [] });

  useEffect(() => {
    (async () => {
      console.log("call getstatic");
      // If this request throws an uncaught error, Next.js will
      // not invalidate the currently shown page and
      // retry getStaticProps on the next request.
      const res = await fetch("https://reqres.in/api/users?page=2");
      const repos = await res.json();
      if (!res.ok) {
        // If there is a server error, you might want to
        // throw an error instead of returning so that the cache is not updated
        // until the next successful request.
        throw new Error(`Failed to fetch posts, received status ${res.status}`);
      }
      setRepo(repos);
    })();
  }, []);
  return (
    <section>
      <h2>Blog</h2>
      <ul>
        {repo ? (
          repo?.data.map(({ id, email, first_name }: Repo) => (
            <li key={id}>
              {email}
              <br />
              {id}
              <br />
              <button onClick={() => alert("call")}>{first_name}</button>
            </li>
          ))
        ) : (
          <p>loading</p>
        )}
      </ul>
    </section>
  );
}
