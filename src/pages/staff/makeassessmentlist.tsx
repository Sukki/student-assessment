import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  CircularProgress,
  Divider,
  FormControlLabel,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import { Controller, useFieldArray, useForm, useWatch } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import { LoadingButton } from "@mui/lab";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";

const CreateAssessment = () => {
  const [showOption, setShowOption] = useState<boolean>(false);
  const [CheckboxData, setCheckbox] = useState<any[]>([]);
  const [isLoading, setisLoading] = useState(true);
  const [saveClicked, setsaveClicked] = useState(false);
  const router = useRouter();
  const methods = useForm<any>({
    mode: "onChange",
    defaultValues: {
      assessmentname: "",
    },
  });

  const { control, handleSubmit, reset, register, getValues } = methods;
  const { fields, append, remove } = useFieldArray<any>({
    control,
    name: "options",
  });

  const optionWatch = useWatch<any>({
    control,
    name: "questiontype",
  });

  const handleGet = async () => {
    axios
      .get("/api/question")
      .then((res) => {
        setisLoading(false);
        setCheckbox(res.data.data);
      })
      .catch((err) => {
        setisLoading(false);
        errorMessage("");
      });
  };
  useEffect(() => {
    if (localStorage.getItem("user") === "staff") {
      handleGet();
    } else {
      errorMessage("");
      localStorage.removeItem("user");
      localStorage.removeItem("userDetails");
      router.push("/");
    }
  }, []);

  useEffect(() => {
    if (optionWatch === "select" || optionWatch === "radio") {
      append({
        name: "",
        value: "",
      });
      setShowOption(true);
    } else {
      setShowOption(false);
      const values = getValues();
      reset({
        ...values,
        options: [],
      });
    }
  }, [optionWatch]);

  const onSubmit = (data: object) => {
    setsaveClicked(true);
    const values = getValues();
    const filtereddata = CheckboxData.filter((e) => e.checked);

    const dataTosend = {
      assessmentname: values.assessmentname,
      questionslist: filtereddata,
      noofquestions: filtereddata.length,
    };
    if (filtereddata.length > 1) {
      axios
        .post("/api/set", { ...dataTosend })
        .then((res) => {
          router.push("/staff/dashboard");
        })
        .catch((err) => {
          setsaveClicked(false);
          errorMessage("");
        });
    } else errorMessage(strings.pleaseselectsomequestions);
  };

  const handleCheckbox = (event: any, index: number) => {
    let obj = [...CheckboxData];
    obj[index]["checked"] = event.target.checked;
    setCheckbox(obj);
  };

  return (
    <Grid container sx={{ display: "flex", justifyContent: "center", mt: 5 }}>
      <Grid lg={10} sm={12} md={12} item>
        <Toaster />
        <Card sx={{ border: `1px solid ${theme}` }}>
          <CardHeader
            title={
              <Typography gutterBottom variant="h5" component="div">
                {strings.makeassessmentquestionset}
              </Typography>
            }
          />
          <Divider />
          <CardContent sx={{ display: "flex", justifyContent: "center" }}>
            <form onSubmit={handleSubmit(onSubmit)}>
              {isLoading ? (
                <CircularProgress disableShrink />
              ) : CheckboxData.length >= 1 ? (
                <>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <TextField
                          sx={{ mb: 3 }}
                          required
                          id="outlined-basic"
                          label={strings.nameofassessment}
                          variant="outlined"
                          fullWidth
                          size="small"
                          {...register(`assessmentname`)}
                          {...field}
                        />
                      )}
                      name={`assessmentname`}
                      control={control}
                    />
                  </Grid>
                  {CheckboxData.map((el, i) => (
                    <Grid key={i} lg={12} sm={12} md={12} item sx={{ mb: 3 }}>
                      <Card sx={{ background: "#F5F6FB" }}>
                        <Controller
                          render={({ field }) => (
                            <CardContent sx={{ display: "flex" }}>
                              <FormControlLabel
                                control={
                                  <Checkbox
                                    onChange={(e: any) => handleCheckbox(e, i)}
                                  />
                                }
                                label={
                                  <Typography
                                    gutterBottom
                                    variant="h5"
                                    component="div"
                                  >
                                    {el.writequestion}
                                  </Typography>
                                }
                              />
                            </CardContent>
                          )}
                          name={`questionlist`}
                          control={control}
                        />
                      </Card>
                    </Grid>
                  ))}
                  <Box sx={{ display: "flex", justifyContent: "center" }}>
                    <Button
                      variant={"outlined"}
                      sx={{ mb: 5, mr: 5, borderColor: theme, color: theme }}
                      onClick={() => router.push("/staff/dashboard")}
                      type="button"
                    >
                      {strings.back}
                    </Button>
                    <LoadingButton
                      loading={saveClicked}
                      loadingIndicator={strings.loading}
                      variant="contained"
                      size="large"
                      sx={{
                        mb: 5,
                        background: theme,
                        "&:hover": {
                          backgroundColor: themehover,
                          color: theme,
                        },
                      }}
                      type="submit"
                    >
                      {strings.create}
                    </LoadingButton>
                  </Box>
                </>
              ) : (
                <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                  <Card sx={{ background: "#F5F6FB" }}>
                    <CardHeader
                      title={
                        <Typography
                          gutterBottom
                          variant="h5"
                          component="div"
                          sx={{ textAlign: "center" }}
                        >
                          {strings.noquestionsavailable}
                        </Typography>
                      }
                    />
                  </Card>
                </Grid>
              )}
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default CreateAssessment;
