import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { Controller, useFieldArray, useForm, useWatch } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import { LoadingButton } from "@mui/lab";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";

const CreateAssessment = () => {
  const [showOption, setShowOption] = useState<boolean>(false);
  const [saveClicked, setsaveClicked] = useState(false);

  const router = useRouter();
  const methods = useForm<any>({
    mode: "onChange",
    defaultValues: {
      questiontype: "radio",
      answer: "",
      writequestion: "",
      options: [{ name: "", value: "" }],
    },
  });

  const { control, handleSubmit, reset, register, getValues } = methods;
  const { fields, append, remove } = useFieldArray<any>({
    control,
    name: "options",
  });

  const optionWatch = useWatch<any>({
    control,
    name: "questiontype",
  });

  useEffect(() => {
    if (localStorage.getItem("user") === "staff") {
      if (optionWatch === "select" || optionWatch === "radio") {
        // append({
        //   name: "",
        //   value: "",
        // });
        setShowOption(true);
      } else {
        setShowOption(false);
        const values = getValues();
        reset({
          ...values,
          options: [],
        });
      }
    } else {
      errorMessage("");
      localStorage.removeItem("user");
      localStorage.removeItem("userDetails");
      router.push("/");
    }
  }, [optionWatch]);

  const onSubmit = async (data: object) => {
    const values = getValues();
    setsaveClicked(true);
    axios
      .post("/api/question", { ...values })
      .then((res) => {
        setsaveClicked(false);
        toast.success(strings.successmsg);
        reset();
      })
      .catch((err) => {
        setsaveClicked(false);
        errorMessage("");
      });
  };

  return (
    <Grid container sx={{ display: "flex", justifyContent: "center", mt: 5 }}>
      <Grid lg={10} sm={12} md={12} item>
        <Toaster />
        <Card sx={{ border: `1px solid ${theme}` }}>
          <CardHeader
            title={
              <Typography gutterBottom variant="h5" component="div">
                {strings.createassessment}
              </Typography>
            }
          />
          <Divider />
          <CardContent>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Box
                sx={{
                  background: "#F5F6FB",
                  padding: 5,
                  borderRadius: 2,
                  marginBottom: "20px",
                }}
              >
                <Grid container spacing={2} sx={{ display: "block" }}>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <FormControl fullWidth size="small">
                          <InputLabel id="demo-simple-select-label">
                            {strings.questiontype}
                          </InputLabel>
                          <Select
                            label={strings.questiontype}
                            {...register(`questiontype`)}
                            {...field}
                            disabled={true}
                          >
                            <MenuItem value={"radio"}>{strings.radio}</MenuItem>
                          </Select>
                        </FormControl>
                      )}
                      name={`questiontype`}
                      control={control}
                    />
                  </Grid>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <TextField
                          id="outlined-basic"
                          label={strings.writequestion}
                          multiline
                          rows={4}
                          variant="outlined"
                          required
                          fullWidth
                          size="small"
                          {...register(`writequestion`)}
                          {...field}
                        />
                      )}
                      name={`writequestion`}
                      control={control}
                    />
                  </Grid>
                  {showOption &&
                    fields.map((item, index) => (
                      <Grid
                        container
                        spacing={2}
                        key={index}
                        sx={{
                          display: "flex",
                          padding: 2,
                        }}
                      >
                        <Grid lg={5} sm={12} md={5} item>
                          <Controller
                            render={({ field }) => (
                              <TextField
                                id="outlined-basic"
                                label={strings.optionslabelname}
                                variant="outlined"
                                required
                                fullWidth
                                size="small"
                                {...register(`options.${index}.name`)}
                              />
                            )}
                            name={`options.${index}.name`}
                            control={control}
                          />
                        </Grid>
                        <Grid lg={5} sm={12} md={5} item>
                          <Controller
                            render={({ field }) => (
                              <TextField
                                id="outlined-basic"
                                label={strings.optionsvalue}
                                variant="outlined"
                                fullWidth
                                required
                                size="small"
                                {...register(`options.${index}.value`)}
                              />
                            )}
                            name={`options.${index}.value`}
                            control={control}
                          />
                        </Grid>
                        <Grid
                          lg={2}
                          sm={12}
                          md={2}
                          item
                          xs={12}
                          sx={{
                            padding: "20px 20px 0px 20px !important",
                            display: "flex",
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button
                            variant={"outlined"}
                            sx={{ borderColor: theme, color: theme }}
                            onClick={() => fields.length > 1 && remove(index)}
                            type="button"
                          >
                            {strings.remove}
                          </Button>
                        </Grid>
                      </Grid>
                    ))}
                  {showOption && (
                    <Box sx={{ display: "flex", justifyContent: "center" }}>
                      <Button
                        variant={"contained"}
                        sx={{
                          background: theme,
                          "&:hover": {
                            backgroundColor: themehover,
                            color: theme,
                          },
                        }}
                        onClick={() =>
                          append({
                            name: "",
                            value: "",
                          })
                        }
                        type="button"
                      >
                        {strings.addoptions}
                      </Button>
                    </Box>
                  )}
                  <Grid lg={12} sm={12} md={12} item>
                    <FormControl fullWidth>
                      <Controller
                        render={({ field }) => (
                          <TextField
                            fullWidth
                            {...field}
                            required
                            size="small"
                            label={strings.answer}
                          />
                        )}
                        name={`answer`}
                        control={control}
                      />
                    </FormControl>
                  </Grid>
                </Grid>
              </Box>

              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <Button
                  variant={"outlined"}
                  sx={{ mb: 1, mr: 5, borderColor: theme, color: theme }}
                  onClick={() => router.push("/staff/dashboard")}
                  type="button"
                >
                  {strings.back}
                </Button>
                <LoadingButton
                  loading={saveClicked}
                  loadingIndicator={strings.loading}
                  variant="contained"
                  size="large"
                  sx={{
                    mb: 1,
                    background: theme,
                    "&:hover": {
                      backgroundColor: themehover,
                      color: theme,
                    },
                  }}
                  type="submit"
                >
                  {strings.create}
                </LoadingButton>
              </Box>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default CreateAssessment;
