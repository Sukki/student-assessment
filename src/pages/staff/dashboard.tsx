import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Grid,
  IconButton,
  Tab,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import DeleteIcon from "@mui/icons-material/Delete";
import toast, { Toaster } from "react-hot-toast";
import { useEffect, useState } from "react";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import axios from "axios";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";
import ReactChart from "@/view/apexchart";

const Dashboard = () => {
  const router = useRouter();
  const [value, setValue] = useState("1");
  const [isLoading, setisLoading] = useState(true);
  const [viewAssessmentData, setviewAssessmentData] = useState([]);
  const [viewAssessmentSetData, setviewAssessmentSetData] = useState([]);
  const [viewAssessmentReportData, setviewAssessmentReportData] = useState<any>(
    []
  );

  const handleChange = (event: any, newValue: any) => {
    setValue(newValue);
  };
  const handleGet = async () => {
    if (value === "1") {
      axios
        .get("/api/question")
        .then((res) => {
          setisLoading(false);
          setviewAssessmentData(res.data.data);
        })
        .catch((err) => {
          setisLoading(false);
          errorMessage("");
        });
    } else if (value === "2") {
      axios
        .get("/api/set")
        .then((res) => {
          setisLoading(false);
          setviewAssessmentSetData(res.data.data);
        })
        .catch((err) => {
          setisLoading(false);
          errorMessage(strings.somethingwentwrong);
        });
    } else if (value === "3") {
      axios
        .get("/api/report")
        .then((res: any) => {
          console.log(res.data, "res.data");
          setviewAssessmentReportData(res.data.reportData);
          setisLoading(false);
        })
        .catch((err) => {
          errorMessage(strings.somethingwentwrong);
          setisLoading(false);
        });
    }
  };
  useEffect(() => {
    if (localStorage.getItem("user") === "staff") {
      setisLoading(true);
      handleGet();
    } else {
      errorMessage(strings.somethingwentwrong);
      localStorage.removeItem("user");
      localStorage.removeItem("userDetails");
      router.push("/");
    }
  }, [value]);

  const handleDelete = (id: string) => {
    setisLoading(true);
    const data = {
      id: id,
    };
    if (value === "1") {
      axios
        .delete("/api/question", { data })
        .then((res) => {
          handleGet();
          toast.success(strings.successfullydeleted);
        })
        .catch((err) => {
          setisLoading(false);
          errorMessage(strings.somethingwentwrong);
        });
    } else if (value === "2") {
      axios
        .delete("/api/set", { data })
        .then((res) => {
          handleGet();
          toast.success(strings.successfullydeleted);
        })
        .catch((err) => {
          setisLoading(false);
          errorMessage(strings.somethingwentwrong);
        });
    }
  };

  return (
    <Box
      sx={{
        width: "85%",
        typography: "body1",
        marginLeft: "auto",
        marginRight: "auto",
        mt: 5,
      }}
    >
      <TabContext value={value}>
        <Card sx={{ border: `1px solid ${theme}` }}>
          <CardHeader
            sx={{
              display: "flex",
              flexWrap: "wrap !important",

              "& .MuiCardHeader-action": {
                display: "contents !important",
                textAlign: "center",
              },
            }}
            title={
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <TabList
                  variant="scrollable"
                  scrollButtons="auto"
                  onChange={handleChange}
                  aria-label="lab API tabs example"
                >
                  <Tab label={strings.questions} value="1" />
                  <Tab label={strings.assessmentsets} value="2" />
                  <Tab label={strings.report} value="3" />
                  <Grid container sx={{ mb: 2 }}>
                    <Toaster />
                  </Grid>
                </TabList>
              </Box>
            }
            action={
              <Box
                sx={{
                  mt: 3,
                  display: "block !important",
                  textAlign: "center",
                }}
              >
                <Button
                  sx={{ mr: 2, mb: 2, borderColor: theme, color: theme }}
                  variant={"outlined"}
                  onClick={() => router.push("/staff/makeassessmentlist")}
                  type="button"
                >
                  {strings.makeassessmentset}
                </Button>
                <Button
                  sx={{ mr: 2, mb: 2, borderColor: theme, color: theme }}
                  variant={"outlined"}
                  onClick={() => router.push("/staff/createassessment")}
                  type="button"
                >
                  {strings.createquestion}
                </Button>
                <Button
                  variant={"contained"}
                  onClick={() => {
                    localStorage.removeItem("user");
                    localStorage.removeItem("userDetails");
                    router.push("/");
                  }}
                  type="button"
                  sx={{
                    mr: 3,
                    mb: 2,
                    background: theme,
                    "&:hover": {
                      backgroundColor: themehover,
                      color: theme,
                    },
                  }}
                >
                  {strings.logout}
                </Button>
              </Box>
            }
          />

          <CardContent>
            <TabPanel value="1">
              <Grid container sx={{ justifyContent: "center" }}>
                {isLoading ? (
                  <CircularProgress disableShrink />
                ) : viewAssessmentData.length >= 1 ? (
                  viewAssessmentData.map((el: any, i: number) => (
                    <Grid
                      key={i}
                      lg={12}
                      sm={12}
                      md={12}
                      item
                      sx={{ mb: 3, width: "100%" }}
                    >
                      <Card sx={{ background: "#F5F6FB" }}>
                        <CardContent sx={{ display: "flex", flexWrap: "wrap" }}>
                          <Grid lg={11} sm={12} md={11} item>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="div"
                            >
                              {i + 1}. {el.writequestion}
                            </Typography>
                          </Grid>
                          <Grid lg={1} sm={12} md={1} item>
                            <IconButton onClick={() => handleDelete(el._id)}>
                              <DeleteIcon />
                            </IconButton>
                          </Grid>
                        </CardContent>
                      </Card>
                    </Grid>
                  ))
                ) : (
                  <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                    <Card sx={{ background: "#F5F6FB" }}>
                      <CardHeader
                        title={
                          <Typography
                            gutterBottom
                            variant="h5"
                            component="div"
                            sx={{ textAlign: "center" }}
                          >
                            {strings.noquestionsavailable}
                          </Typography>
                        }
                      />
                    </Card>
                  </Grid>
                )}
              </Grid>
            </TabPanel>
            <TabPanel value="2">
              <Grid container sx={{ justifyContent: "center" }}>
                {isLoading ? (
                  <CircularProgress disableShrink />
                ) : viewAssessmentSetData.length >= 1 ? (
                  viewAssessmentSetData.map((el: any, i: number) => (
                    <Grid key={i} lg={12} sm={12} md={12} item sx={{ mb: 3 }}>
                      <Card sx={{ background: "#F5F6FB" }}>
                        <CardContent sx={{ display: "flex" }}>
                          <Grid lg={11} sm={12} md={11} item>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="div"
                            >
                              {i + 1}. {el.assessmentname} - {el.noofquestions}{" "}
                              {strings.questions.toLowerCase()}
                            </Typography>
                          </Grid>
                          <Grid lg={1} sm={12} md={1} item>
                            <IconButton onClick={() => handleDelete(el._id)}>
                              <DeleteIcon />
                            </IconButton>
                          </Grid>
                        </CardContent>
                      </Card>
                    </Grid>
                  ))
                ) : (
                  <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                    <Card sx={{ background: "#F5F6FB" }}>
                      <CardHeader
                        title={
                          <Typography
                            gutterBottom
                            variant="h5"
                            component="div"
                            sx={{ textAlign: "center" }}
                          >
                            {strings.noassessmentavailable}
                          </Typography>
                        }
                      />
                    </Card>
                  </Grid>
                )}
              </Grid>
            </TabPanel>
            <TabPanel value="3">
              <Grid container sx={{ justifyContent: "center" }}>
                {isLoading ? (
                  <CircularProgress disableShrink />
                ) : viewAssessmentReportData?.hasOwnProperty("reportData") ? (
                  <Grid lg={12} sm={12} md={12} item sx={{ mb: 3 }}>
                    <Card sx={{ background: "#F5F6FB" }}>
                      <CardContent
                        sx={{ display: "flex", justifyContent: "center" }}
                      >
                        <ReactChart data={viewAssessmentReportData} />
                      </CardContent>
                    </Card>
                  </Grid>
                ) : (
                  <Grid lg={12} sm={12} md={12} item sx={{ mt: 5 }}>
                    <Card sx={{ background: "#F5F6FB" }}>
                      <CardHeader
                        title={
                          <Typography
                            gutterBottom
                            variant="h5"
                            component="div"
                            sx={{ textAlign: "center" }}
                          >
                            {strings.noreportavailable}
                          </Typography>
                        }
                      />
                    </Card>
                  </Grid>
                )}
              </Grid>
            </TabPanel>
          </CardContent>
        </Card>
      </TabContext>
    </Box>
  );
};
export default Dashboard;
