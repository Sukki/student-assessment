import { MongoClient, ObjectId } from "mongodb";

export default async function handler(req, res) {
  const client = new MongoClient(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await client.connect();
  const database = client.db("student-assessment"); // Choose a name for your database

  const questioncollection = database.collection("student-assessment-set"); // Choose a name for your collection

  if (req.method === "POST") {
    try {
      const datas = await questioncollection.findOne({
        _id: ObjectId(req.body.id),
      });
      res.json({
        message: "Data got successfully!",
        status: 200,
        data: datas,
      });
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else {
    res.status(405).json({ message: "Method not allowed!" });
  }
}
