import { MongoClient, ObjectId } from "mongodb";

export default async function handler(req, res) {
  const client = new MongoClient(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await client.connect();
  const database = client.db("student-assessment"); // Choose a name for your database

  const questioncollection = database.collection("student-assessment-set"); // Choose a name for your collection

  if (req.method === "POST") {
    const data = req.body;

    try {
      await questioncollection.insertOne({ ...data });

      res.status(200).json({ message: "Data saved successfully!" });
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else if (req.method === "GET") {
    try {
      const datas = await questioncollection.find({}).toArray();
      const count = await questioncollection.countDocuments({});

      // const count = await this.testModel.countDocuments({}).exec();
      // const page_total = Math.floor((count - 1) / limit) + 1;
      // const data = await this.testModel.find().limit(limit).skip(skip).exec();
      res.json({
        message: "Data got successfully!",
        status: 200,
        data: datas,
        count: count,
      });
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else if (req.method === "DELETE") {
    try {
      await questioncollection.deleteOne({
        _id: ObjectId(req.body.id),
      });
      res.json({
        message: "Data deleted successfully!",
        status: 200,
      });
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else {
    res.status(405).json({ message: "Method not allowed!" });
  }
}
