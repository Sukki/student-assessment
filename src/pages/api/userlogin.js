import { MongoClient, ObjectId } from "mongodb";

export default async function handler(req, res) {
  const client = new MongoClient(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await client.connect();
  const database = client.db("student-assessment"); // Choose a name for your database

  const User = database.collection("users"); // Choose a name for your collection

  if (req.method === "POST") {
    const data = req.body;

    try {
      const user = await User.findOne({ username: data.username });
      if (user) {
        //check if password matches
        const result = data.password === user.password;
        if (result) {
          res
            .status(200)
            .json({ message: "User Login successfully!", data: { ...user } });
        } else {
          res.status(400).json({ error: "Invalid Credentials" });
        }
      } else {
        res.status(400).json({ error: "User doesn't exist" });
      }
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else {
    res.status(405).json({ message: "Method not allowed!" });
  }
}
