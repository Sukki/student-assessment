import { MongoClient, ObjectId } from "mongodb";

export default async function handler(req, res) {
  const client = new MongoClient(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await client.connect();
  const database = client.db("student-assessment"); // Choose a name for your database

  const User = database.collection("users"); // Choose a name for your collection

  if (req.method === "POST") {
    const data = req.body;

    try {
      await User.insertOne({ ...data });

      res.status(200).json({ message: "User Registered successfully!" });
    } catch (error) {
      res.status(500).json({ message: "Something went wrong!" });
    } finally {
      await client.close();
    }
  } else {
    res.status(405).json({ message: "Method not allowed!" });
  }
}
