import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { Controller, useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import { LoadingButton } from "@mui/lab";
import { errorMessage, theme, themehover } from "@/utils/utils";
import { strings } from "@/utils/strings";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const inputStyle = { WebkitBoxShadow: "0 0 0 transparent white inset" };

const Register = () => {
  const [saveClicked, setsaveClicked] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const router = useRouter();
  const methods = useForm<any>({
    mode: "onChange",
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const { control, handleSubmit, reset, register, getValues } = methods;

  const onSubmit = async (data: object) => {
    const values = getValues();
    setsaveClicked(true);
    axios
      .post("/api/register", { ...values })
      .then((res) => {
        setsaveClicked(false);
        toast.success(strings.pleaselogin);
        router.push("/");
      })
      .catch((err) => {
        setsaveClicked(false);
        errorMessage(err?.response?.data?.error);
      });
  };

  return (
    <Grid container sx={{ display: "flex", justifyContent: "center", mt: 5 }}>
      <Grid lg={6} sm={12} md={6} item>
        <Toaster />
        <Card
          sx={{
            background: "#F5F6FB",
            padding: 2,
            borderRadius: 2,
            marginBottom: "20px",
            border: `1px solid ${theme}`,
          }}
        >
          <CardHeader
            title={
              <Typography
                gutterBottom
                variant="h4"
                component="div"
                sx={{ textAlign: "center" }}
              >
                {strings.register}
              </Typography>
            }
          />

          <CardContent>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Box>
                <Grid container spacing={4} sx={{ display: "block" }}>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <TextField
                          id="outlined-basic"
                          label={strings.firstname}
                          variant="outlined"
                          required
                          fullWidth
                          size="small"
                          {...register(`username`)}
                          {...field}
                        />
                      )}
                      name={`username`}
                      control={control}
                    />
                  </Grid>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <TextField
                          id="outlined-basic"
                          label={strings.lastname}
                          variant="outlined"
                          fullWidth
                          size="small"
                          {...register(`lastname`)}
                          {...field}
                        />
                      )}
                      name={`lastname`}
                      control={control}
                    />
                  </Grid>
                  <Grid lg={12} sm={12} md={12} item>
                    <FormControl fullWidth>
                      <Controller
                        render={({ field }) => (
                          <>
                            <input
                              name="abc"
                              type="text"
                              readOnly={true}
                              style={{ display: "none" }}
                            />
                            <TextField
                              autoComplete="new-password"
                              fullWidth
                              inputProps={{ style: inputStyle }}
                              onChange={field.onChange}
                              required
                              type={showPassword ? "text" : "password"}
                              size="small"
                              label={strings.password}
                              sx={{
                                '& input[type="password"]::-ms-reveal': {
                                  display: "none",
                                },
                                '& input[type="password"]::-ms-clear': {
                                  display: "none",
                                },
                              }}
                              InputProps={{
                                endAdornment: (
                                  <InputAdornment position="end">
                                    <IconButton
                                      aria-label="toggle password visibility"
                                      onClick={handleClickShowPassword}
                                      onMouseDown={handleMouseDownPassword}
                                      edge="end"
                                    >
                                      {showPassword ? (
                                        <Visibility />
                                      ) : (
                                        <VisibilityOff />
                                      )}
                                    </IconButton>
                                  </InputAdornment>
                                ),
                              }}
                            />
                          </>
                        )}
                        name={`password`}
                        control={control}
                      />
                    </FormControl>
                  </Grid>
                  <Grid lg={12} sm={12} md={12} item>
                    <Controller
                      render={({ field }) => (
                        <FormControl fullWidth size="small">
                          <InputLabel id="demo-simple-select-label">
                            {strings.role}*
                          </InputLabel>
                          <Select
                            label={strings.role}
                            {...register(`role`)}
                            {...field}
                            required
                          >
                            <MenuItem value={"student"}>
                              {strings.student}
                            </MenuItem>
                            <MenuItem value={"staff"}>{strings.staff}</MenuItem>
                          </Select>
                        </FormControl>
                      )}
                      name={`role`}
                      control={control}
                    />
                  </Grid>
                </Grid>
              </Box>

              <Box sx={{ display: "flex", justifyContent: "center", mt: 3 }}>
                <Button
                  variant={"outlined"}
                  sx={{ mb: 5, mr: 5, borderColor: theme, color: theme }}
                  onClick={() => router.push("/")}
                  type="button"
                >
                  {strings.back}
                </Button>
                <LoadingButton
                  loading={saveClicked}
                  loadingIndicator={strings.loading}
                  variant="contained"
                  size="large"
                  sx={{
                    mb: 5,
                    background: theme,
                    "&:hover": {
                      backgroundColor: themehover,
                      color: theme,
                    },
                  }}
                  type="submit"
                >
                  {strings.register}
                </LoadingButton>
              </Box>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default Register;
