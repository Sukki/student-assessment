import dynamic from "next/dynamic";
const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false,
});

const ReactChart = ({ data }: any) => {
  data?.reportData.push("totalAttended");
  data?.seriesData?.push(data?.totalAttended);
  const series: any = data?.seriesData;
  //   const colors = ["#FFCEA1", "#AC314D", "#C677FF"];
  const options: any = {
    chart: {
      type: "donut",
    },
    // colors: colors,
    labels: data?.reportData,
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <>
      <div id="chart">
        <ReactApexChart
          options={options}
          series={series}
          type="donut"
          width={500}
          height={320}
        />
      </div>
    </>
  );
};

export default ReactChart;
