import toast from "react-hot-toast";
import { strings } from "./strings";

export const errorMessage = (message: string) => {
  return toast.error(message ? message : strings.somethingwentwrong);
};

export const theme = "black";
export const themehover = "white";
