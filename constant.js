export const createAssessmentData = {
  questiontype: "radio",
  answer: "cxc",
  writequestion: "cxcx",
  checked: false,
  options: [
    {
      name: "cxc",
      value: "cxc",
    },
    {
      name: "rfef",
      value: "cx",
    },
  ],
};

export const viewAssessmentSetData = [
  {
    setname: "SetA",
    noofquestions: 5,
  },
  {
    setname: "SetB",
    noofquestions: 5,
  },
];

export const viewAssessmentData = [
  {
    questiontype: "radio",
    answer: "cxc",
    checked: false,
    writequestion:
      "new cnjhbgvhda sdahbsjasd adsvgbhjn xcdhbasj new cnjhbgvhda sdahbsjasd adsvgbhjn xcdhbasjnew cnjhbgvhda sdahbsjasd adsvgbhjn xcdhbasj",
    options: [
      {
        name: "cxc",
        value: "cxc",
      },
      {
        name: "rfef",
        value: "cx",
      },
    ],
  },
  {
    questiontype: "radio",
    answer: "cxc",
    checked: false,
    writequestion: "test",
    options: [
      {
        name: "cxc",
        value: "cxc",
      },
      {
        name: "rfef",
        value: "cx",
      },
    ],
  },
];
